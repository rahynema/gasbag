import ROOT

########## My Functions ########## 

# Here, define relevant analytic functions which describe your background shape. These 
# functions will be used for optimizing hyper-parameters, running toy tests, and defining 
# the GP mean (initial guess of the background shape). Most likely they should be the 
# functions used for the spurious signal test. 
# Define these functions using RooFactory notation, along with the name of the function:
#   myBkgFunctions.append( [ "FUNCTION_NAME" , "FUNCTION_RooFactory_DEFINITION" ] )
# Be sure to name the background function "bkg" (i.e. EXPR:bkg(...))
# Below, you may also define signal shape functions. Note that these are only used to 
# optimize the hyper parameters, and to run toy tests for validation. This code does NOT 
# attempt to extract a real signal from an input distribution. 


# Can comment this out if not using double-sided crystal ball shape 
ROOT.gROOT.LoadMacro("inc/HggTwoSidedCBPdf.cxx+") 


def define_background_functions():

	myBkgFunctions = [] 
	
	########## Begin function definitions ##########

	# PowerLaw 
	myBkgFunctions.append( [ "PowerLaw" , "EXPR:bkg('rooMyy**PowerLaw_b',{rooMyy,PowerLaw_b[-1.447,-20,20]})" ] )

	# Exponential 	
	myBkgFunctions.append( [ "Exponential" , "RooExponential:bkg(rooMyy, Exponential_b[-0.02, -0.5, 0.0])" ] ) 

	# ExpPoly2 
	myBkgFunctions.append( [ "ExpPoly2" , "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly2_b+ExpPoly2_c*(rooMyy-100)/100))',{rooMyy,ExpPoly2_b[-2.0,-10,10],ExpPoly2_c[1.0,-10,10]})" ] ) 

	# ExpPoly3 
	myBkgFunctions.append( [ "ExpPoly3" , "EXPR:bkg('exp((rooMyy-100)/100*(ExpPoly3_b+ExpPoly3_c*(rooMyy-100)/100+ExpPoly3_d*(rooMyy-100)*(rooMyy-100)/(100*100)))',{rooMyy,ExpPoly3_b[-3.7,-10,10],ExpPoly3_c[2.0,-10,10],ExpPoly3_d[0.5,-10,10]})" ] ) 

	# Bern3 
	myBkgFunctions.append( [ "Bern3" , "RooBernstein:bkg(rooMyy, {Bern3_b[5,-10,10], Bern3_c[2,-10,10], Bern3_d[2,-10,10], 1})" ] ) 

	# Bern4 
	myBkgFunctions.append( [ "Bern4" , "RooBernstein:bkg(rooMyy, {Bern4_b[9,-15,15], Bern4_c[4,-10,10], Bern4_d[3,-10,10], Bern4_e[2,-10,10], 1})" ] ) 

	# Bern5 
	myBkgFunctions.append( [ "Bern5" , "RooBernstein:bkg(rooMyy, {Bern5_b[10,-15,15], Bern5_c[7.5,-10,10], Bern5_d[5.5,-10,10], Bern5_e[3,-10,10], Bern5_f[2,-10,10], 1})" ] ) 

	########## End function definitions ##########

	return myBkgFunctions 
	
	
	
def define_signal_functions():

	mySigFunctions = [] 
	
	########## Begin function definitions ##########

	# Crystal Ball 
	mySigFunctions.append( [ "CrystalBall" , "RooCBShape:sig(rooMyy, RooM0[0,30000], sigWidth[2,0,10], sigAlpha[-5,-20,20], sigN[5,0,50] )" ] )
	
	# H(yy) Double-Sided Crystal Ball 
	mySigFunctions.append( [ "HggDSCB" , "HggTwoSidedCBPdf:sig(rooMyy, RooM0[0,30000], sigWidth[2,0,10], sigAlphaLo[5,0,20], sigAlphaHi[5,0,20], sigNLo[5,0,50], sigNHi[5,0,50] )" ] ) 

	########## End function definitions ##########

	return mySigFunctions 	

