# Script written by Rachel Hyneman, 2019
# Contact: rachel.hyneman@cern.ch

import argparse
import random as rand
import numpy as np
import ROOT
import sys
sys.path.append('inc/')
from utils import generateBackgroundWorkspace, generateSignalWorkspace, getInputXAxis, makeBinning, makeGPtemplate, define_background_functions, define_signal_functions , fluctuateTemplate, performSBFit


########### User Defined Options ##########

# These are all pulled from the inc/inputs.py file

import inputs as userin


###########################################
########### The Important Stuff ###########

def main( isPrepMode ):

	#import pdb; pdb.set_trace()
	#get_user_inputs( userin )

	if( isPrepMode ):
		print("Running in PREPARATION mode! This means that the input template(s) will be fit with an analytic function. This function will be saved to a file and sourced later for generating toys.")
		if( userin.outToyBasisFileName ):
			print("   Saving toy shapes to: ",userin.outToyBasisFileName)
		else:
			print("   ERROR! No output toy shape basis file specified (outToyBasisFileName) in inputs.py")
		outToyBasisFile = ROOT.TFile( userin.outToyBasisFileName , "RECREATE" )

	if( not hasattr(userin, 'templateHistoNames') ):
		print("ERROR! No template histogram names given (templateHistoNames). Nothing to smooth!")
		return

	templateHistoNames = userin.templateHistoNames
	nCategories = len(userin.templateHistoNames)

	signalWidths = []
	signalPositions = []
	nSignalInject = []
	signalPDFs = []
	signalDataSets = []
	signalInjectFractions = []

	sigWidthMultipliers = []
	toyYieldOverrides = []

	useSignalFracs = False

	categoryNames = ""
	if( hasattr(userin, 'categoryNames') ): categoryNames = userin.categoryNames
	if ( len(categoryNames) != nCategories ):
		print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
		categoryNames = templateHistoNames

	hyperParFileName = ""
	if( hasattr(userin, 'hyperParFileName') ): hyperParFileName = userin.hyperParFileName
	if( not not hyperParFileName ):
		print("INFO: Opening hyper-parameter file ",hyperParFileName)
		hyperParFile = ROOT.TFile.Open(hyperParFileName)

	### Extract the information from the signal parameterization file
	if( not hasattr(userin, 'inSignalFileName') ): userin.inSignalFileName = ""
	if( not not userin.inSignalFileName ): # if a file name is provided, extract info from this

		# Need to do this in case the signal shape is non-standard (myFunctions.py should contain call to compile custom class)
		define_signal_functions()

		inSignalFile = ROOT.TFile.Open(userin.inSignalFileName,"READ")
		signalFileWS = inSignalFile.Get(userin.inSignalWorkspaceName)

		for iCat in range(nCategories):
			signalPDFs.append( signalFileWS.pdf( userin.signalFile_sigPDFNames[iCat] ) )
			signalDataSets.append( signalFileWS.data(userin.signalFile_sigDataNames[iCat]) )

			sigPDF_name = userin.signalFile_sigPDFNames[iCat]
			pars = signalPDFs[iCat].getParameters( signalDataSets[iCat] )
			variter = pars.createIterator()
			whichvar = variter.Next()

			# Dummy names
			sigPDF_widthName = "dummy"
			sigPDF_posName = "dummy"

			isMassDepWidth = False

			while whichvar:
				print( whichvar.GetName() )
				if( userin.signalFile_sigWidthNametag in whichvar.GetName() ):
					sigPDF_widthName = whichvar.GetName()
				elif( userin.signalFile_sigMeanNametag in whichvar.GetName() ):
					sigPDF_posName = whichvar.GetName()
				whichvar = variter.Next()

			# If the above does not work, we may be looking at RooFormulaVars, which are saved under the workspace and not the PDF
			if( ( sigPDF_widthName == "dummy" ) or ( sigPDF_posName == "dummy" ) ):

				funcs = signalFileWS.allFunctions()
				variter = funcs.createIterator()
				whichvar = variter.Next()

				while whichvar:
					print( whichvar.GetName() )
					if( userin.signalFile_sigWidthNametag in whichvar.GetName() ):
						sigPDF_widthName = whichvar.GetName()
					elif( userin.signalFile_sigMeanNametag in whichvar.GetName() ):
						sigPDF_posName = whichvar.GetName()
					whichvar = variter.Next()

				isMassDepWidth = True


			if( sigPDF_widthName == "dummy" ):
				print("ERROR! Could not find a variable matching '"+userin.signalFile_sigWidthNametag+"' in file '"+userin.inSignalFileName+"'. Check signalFile_sigWidthNametag in inputs.py .")
				return 2
			if( sigPDF_posName == "dummy" ):
				print("ERROR! Could not find a variable matching '"+userin.signalFile_sigMeanNametag+"' in file '"+userin.inSignalFileName+"'. Check signalFile_sigMeanNametag in inputs.py .")
				return 2
			try: sigPDF_yieldName = userin.signalFile_sigYieldNames[iCat]
			except:
				print("ERROR! Could not find a variable matching '"+userin.signalFile_sigYieldNames[iCat]+"' in file '"+userin.inSignalFileName+"'. Check signalFile_sigYieldNametag in inputs.py .")
				return 2

			signalWidths.append( (signalFileWS.obj(sigPDF_widthName)).getVal() )
			signalPositions.append( (signalFileWS.obj(sigPDF_posName)).getVal() )

			if( ( not hasattr(userin, 'nSignalInject') ) and ( not hasattr(userin, 'signalInjectFractions') ) ):
				nSignalInject.append( (signalFileWS.obj(sigPDF_yieldName)).getVal() )
			else:
				print("INFO: User has defined nSignalInject or signalInjectFractions in inputs.py. The nominal yields in file '"+userin.inSignalFileName+"' will be ignored.")
				if( ( not hasattr(userin, 'nSignalInject') ) and ( hasattr(userin, 'signalInjectFractions') ) ):
					useSignalFracs = True
					if ( len(userin.signalInjectFractions) != nCategories ):
						print("NOTICE - The number of injected signal fractions does not equal the number of categories. The first yield will be injected into all categories.")
						signalInjectFractions = [ userin.signalInjectFractions[0] for i in range(nCategories) ]
					else: signalInjectFractions = userin.signalInjectFractions
					nSignalInject = [ -1.0 for i in range(nCategories) ]
				elif ( len(userin.nSignalInject) != nCategories ):
					print("NOTICE - The number of injected signal yields does not equal the number of categories. The first yield will be injected into all categories.")
					nSignalInject = [ userin.nSignalInject[0] for i in range(nCategories) ]
				else: nSignalInject = userin.nSignalInject

		if( len(signalWidths) != nCategories or len(signalPositions) != nCategories or len(nSignalInject) != nCategories ):
			print( "ERROR - The provided signal parameterization file doesn't contain the same number of signal shapes as the number of categories")
			print( "        Try checking the signalFile_sigXXParameters lists to ensure there are no missing entries" )

	else: ### Extract the information from the values provided in the input file
			if( ( not hasattr(userin, 'nSignalInject') ) and ( hasattr(userin, 'signalInjectFractions') ) ):
				useSignalFracs = True
				if ( len(userin.signalInjectFractions) != nCategories ):
					print("NOTICE - The number of injected signal fractions does not equal the number of categories. The first yield will be injected into all categories.")
					signalInjectFractions = [ userin.signalInjectFractions[0] for i in range(nCategories) ]
				else: signalInjectFractions = userin.signalInjectFractions
				nSignalInject = [ -1.0 for i in range(nCategories) ]
			elif ( len(userin.nSignalInject) != nCategories ):
				print("NOTICE - The number of injected signal yields does not equal the number of categories. The first yield will be injected into all categories.")
				nSignalInject = [ userin.nSignalInject[0] for i in range(nCategories) ]
			else: nSignalInject = userin.nSignalInject

			signalWidths = userin.signalWidths
			signalPositions = userin.signalPositions

			# If only one parameter passed for injected "signal", use for all categories
			if ( len(userin.signalWidths) == 1 ): signalWidths = [ userin.signalWidths[0] for i in range(nCategories) ]
			if ( len(userin.signalPositions) == 1 ): signalPositions = [ userin.signalPositions[0] for i in range(nCategories) ]

			# If multiple parameters passed, need to make sure the # of parameters is the same as the # of categories. Otherwise, break!
			if ( len(signalWidths) != nCategories ):
				print( 	"WARNING - The number of signal widths passed is not the same as the number of background templates passed! Breaking." )
				return 1
			elif ( len(signalPositions) != nCategories ):
				print( 	"WARNING - The number of signal x-positions passed is not the same as the number of background templates passed! Breaking." )
				return 1
			elif ( len(nSignalInject) != nCategories ):
				print( 	"WARNING - The number of signal yields passed is not the same as the number of background templates passed! Breaking." )
				return 1

	useSigWidthMultiplier = False
	if( hasattr(userin, 'sigWidthMultiplier') ):
		useSigWidthMultiplier = True
		sigWidthMultipliers = userin.sigWidthMultiplier
		if( len(sigWidthMultipliers) != nCategories ):
			sigWidthMultipliers = [ userin.sigWidthMultiplier[0] for i in range(nCategories) ]

	useToyYieldOverride = False
	if( hasattr(userin, 'toyYieldOverride') ):
		useToyYieldOverride = True
		toyYieldOverrides = userin.toyYieldOverride
		if( len(toyYieldOverrides) != nCategories ):
			toyYieldOverrides = [ userin.toyYieldOverride[0] for i in range(nCategories) ]

	useToySigPosOverride = False
	if( hasattr(userin, 'toyInjSigPosOverride') ):
		useToySigPosOverride = True
		toyInjSigPosOverrides = userin.toyInjSigPosOverride
		if( len(toyInjSigPosOverrides) != nCategories ):
			toyInjSigPosOverrides = [ userin.toyInjSigPosOverrides[0] for i in range(nCategories) ]

	if( hasattr(userin, 'toyMassScanPoints') ): toyMassScanPoints = userin.toyMassScanPoints
	else: toyMassScanPoints = []

	if( not isPrepMode ):
		outToyTestFile = ROOT.TFile(userin.outToyTestFileName,"RECREATE")
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C")
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C")
	ROOT.SetAtlasStyle()

	if( hasattr(userin, 'outToyBasisFileName') ): outToyBasisFileName = userin.outToyBasisFileName
	else: outToyBasisFileName = ""
	if( isPrepMode and ( not outToyBasisFileName ) ):
		print("ERROR! Preparation mode requested (creating file with analytic fits to inputs for toy generation). But, no output filename (outToyBasisFileName) was provided. Breaking.")
		return 3


	for iCat in range(nCategories):

		### Setup all inputs before applying GPR

		inHistName = templateHistoNames[ iCat ]
		if( len(categoryNames) ): catName = categoryNames[ iCat ]
		else: catName = inHistName

		print( "Running Toys for Category: ", catName )


		### Get the input template histogram

		inFile = ROOT.TFile.Open(userin.inFileName,"READ")
		inHisto = inFile.Get(inHistName)

		if( not inHisto ):
			print( "ERROR - Template '"+inHistName+"' not found!! Check template name and file contents." )
			continue

		nRebin = 1
		if( hasattr(userin, 'nRebin') ): nRebin = userin.nRebin
		if( nRebin != 1 ): inHisto.Rebin( nRebin )

		# If we need to, trim the input histogram
		gpFitRange = [ -9999 , -9999 ]
		if( hasattr(userin, 'massRange') ):

			gpFitRange = userin.massRange
			oldBins = np.array([],"d")
			newBins = np.array([],"d")

			for ibin in range(inHisto.GetNbinsX()+1):
				oldBins = np.append( oldBins , inHisto.GetBinLowEdge(ibin+1) )

			for binEdge in oldBins:
				if( binEdge < gpFitRange[0] ): continue
				if( binEdge > gpFitRange[1] ): continue
				newBins = np.append( newBins , binEdge )

			dummyTrimmedHisto = ROOT.TH1D( inHistName+'_trim' , inHistName+'_trim' , len(newBins)-1 , newBins )
			for ibin in range(len(newBins)):
				newBinCenter = dummyTrimmedHisto.GetBinCenter(ibin+1)
				oldBin = inHisto.FindBin( newBinCenter )
				dummyTrimmedHisto.SetBinContent( ibin , inHisto.GetBinContent(oldBin) )
				dummyTrimmedHisto.SetBinError( ibin , inHisto.GetBinError(oldBin) )

			inHisto = dummyTrimmedHisto
			inHisto.SetNameTitle(inHistName,inHistName)

		xMin = inHisto.GetBinLowEdge(1)
		xMax = inHisto.GetBinLowEdge( inHisto.GetNbinsX()+1 )

		myBins = makeBinning(inHisto) # the bin edges (for defining histograms)
		nBins = inHisto.GetNbinsX()

		bkgYield = inHisto.Integral()


		# Get the kernel hyper-parameters from the input file

		length_scale_bounds = [-1,-1]
		length_scale_slope_bounds = [-1,-1]

		if( not not hyperParFileName ):
			hyperParTree = hyperParFile.Get( "HyperParArea_"+inHistName )
			hyperParTree.GetEntry(0)
			length_scale_bounds[0] =  hyperParTree.GetLeaf("length_scale_min").GetValue()
			length_scale_bounds[1] =  hyperParTree.GetLeaf("length_scale_max").GetValue()
			length_scale_slope_bounds[0] =  hyperParTree.GetLeaf("length_scale_slope_min").GetValue()
			length_scale_slope_bounds[1] =  hyperParTree.GetLeaf("length_scale_slope_max").GetValue()

			if( length_scale_bounds[0] < 0.0 or length_scale_bounds[1] < 0.0 ):
				print("ERROR - length scale bounds are invalid. The optimization may have failed.")
				print("   Try providing bounds manually in the input file (length_scale_bounds) and commenting out the hyperParFileName. ")
				print("   Will skip this category!! ")
				break
		elif( hasattr(userin, 'length_scale_bounds') ):
			length_scale_bounds = userin.length_scale_bounds
			length_scale_slope_bounds = userin.length_scale_slope_bounds
			if( length_scale_bounds[0] < 0.0 or length_scale_bounds[1] < 0.0 ):
				print("ERROR - the length scale bounds specified in the input file (length_scale_bounds) are invalid.")
				print("   Make sure these values are greater than 0. ")
				print("   Will skip this category!! ")
				break
		else:
			print("ERROR - no hyper-parameter bounds or file (length_scale_bounds/hyperParFileName) are provided.")
			return

		length_scale = length_scale_bounds[0] + 0.5*( length_scale_bounds[1] - length_scale_bounds[0] )
		length_scale_slope = length_scale_slope_bounds[0] + 0.5*( length_scale_slope_bounds[1] - length_scale_slope_bounds[0] )

		# Check remaining user inputs

		nBinsToPad = 0
		if( hasattr(userin, 'nBinsToPad') ): nBinsToPad = userin.nBinsToPad

		if( hasattr(userin, 'whichKernel') ): whichKernel = userin.whichKernel
		else: print("ERROR! No kernel (whichKernel) specified in the input file!")

		if( hasattr(userin, 'whichGPMean') ): whichGPMean = userin.whichGPMean
		else: print("ERROR! No GP mean (whichGPMean) specified in the input file!")

		if( hasattr(userin, 'whichErrorTreatment') ): whichErrorTreatment = userin.whichErrorTreatment
		else: print("ERROR! No input error treatment (whichErrorTreatment) specified in the input file!")

		if( hasattr(userin, 'whichErrorOutput') ): whichErrorOutput = userin.whichErrorOutput
		else: print("ERROR! No output error treatment (whichErrorOutput) specified in the input file!")

		sbFitMassRange = [ -9999 , -9999 ]
		if( hasattr(userin, 'sbFitMassRange') ): sbFitMassRange = userin.sbFitMassRange

		### Get the Signal Shape information

		# Some default values
		sigPDF_name = "sig"
		sigPDF_widthName = "sigWidth"
		sigPDF_posName = "RooM0"
		sigPDF_obsName = "rooMyy"

		# If signal workspace has been pulled from an input file, need to get the names of the parameters
		if( len(signalPDFs) > 0 ):

			sigPDF_obsName = ( signalDataSets[iCat].get() ).begin().GetName()
			signalDataSets[iCat].changeObservableName( sigPDF_obsName , "rooMyy" )

			signalPDF = signalPDFs[iCat]
			sigPDF_name = signalPDF.GetName()
			pars = signalPDF.getParameters( signalDataSets[iCat] )
			variter = pars.createIterator()
			whichvar = variter.Next()

			while whichvar:
				if( userin.signalFile_sigWidthNametag in whichvar.GetName() ):
					sigPDF_widthName = whichvar.GetName()
				if( userin.signalFile_sigMeanNametag in whichvar.GetName() ):
					sigPDF_posName = whichvar.GetName()
				if( whichvar.GetName() == sigPDF_obsName ):
					whichvar.SetName("rooMyy")
					whichvar.setRange(xMin,xMax)
				whichvar = variter.Next()

			# If the above does not work, we may be looking at RooFormulaVars, which are saved under the workspace and not the PDF

			if( ( sigPDF_widthName == "sigWidth" ) or ( sigPDF_posName == "RooM0" ) ):

				funcs = signalFileWS.allFunctions()
				variter = funcs.createIterator()
				whichvar = variter.Next()

				while whichvar:
					print( whichvar.GetName() )
					if( userin.signalFile_sigWidthNametag in whichvar.GetName() ):
						sigPDF_widthName = whichvar.GetName()
					elif( userin.signalFile_sigMeanNametag in whichvar.GetName() ):
						sigPDF_posName = whichvar.GetName()
					whichvar = variter.Next()

			basisSigWS = ROOT.RooWorkspace("signalWS")
			getattr(basisSigWS,'import')( signalPDF )

		else: basisSigWS = generateSignalWorkspace( "signalWS" , userin.whichSigShape , [ xMin , xMax ] )

		# Extract the signal shape for this category from the input signal file

		signalWidth = signalWidths[iCat]
		if( useSigWidthMultiplier ): signalWidth *= sigWidthMultipliers[iCat]
		signalPos = signalPositions[iCat]

		if( nSignalInject[iCat] < 0 ): nSignalInject[iCat] =  bkgYield * signalInjectFractions[iCat]
		sigYield = nSignalInject[iCat]

		if( not isMassDepWidth ):
			basisSigWS.var(sigPDF_widthName).setVal( signalWidth )
			basisSigWS.var(sigPDF_posName).setVal( signalPos )
		else:
			if( useSigWidthMultiplier ):
				print("WARNING! Use of the Signal Width Multiplier option for injected signals is not (yet) supported with a mass-dependent signal shape.")
			basisSigWS.var(sigPDF_posName).setVal( signalPos )


		### Next, get the background shape information

		# If a toy basis file has been provided, use these workspaces
		if( ( not isPrepMode ) and outToyBasisFileName ):
			print("Obtaining basis for generating toys from file: ",outToyBasisFileName)
			outToyBasisFile = ROOT.TFile.Open( outToyBasisFileName )
			basisBkgWS = outToyBasisFile.Get( "basisBkgWS_"+str(inHistName) )

		# Otherwise, need to generate these basis shapes and store in a workspace
		else:

			### Determine the Optimal Analytical Function to Use
			# Want to model the shape of the template, but without any statistical fluctuations

			print("Obtaining basis for generating toys - will test functions in inc/myFunctions.py")

			myBkgFunctions = define_background_functions()
			nDOF = np.array([],'i')
			allChiSqrd = np.array([],'d')

			for i,funcDef in enumerate(myBkgFunctions):
				testFunction = funcDef[0]
				testBkgWS = generateBackgroundWorkspace( "test"+testFunction+"WS" , testFunction , [ xMin , xMax ] )
				testerRooHist = ROOT.RooDataHist("testerRooHist", "testerRooHist", ROOT.RooArgList( testBkgWS.obj("rooMyy") ), inHisto)
				testBkgWS.pdf("bkg").fitTo( testerRooHist , ROOT.RooFit.SumW2Error(ROOT.kTRUE) , ROOT.RooFit.PrintLevel(0) , ROOT.RooFit.PrintEvalErrors(2) )
				testFrame = testBkgWS.obj('rooMyy').frame()
				testerRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(1) )
				testBkgWS.pdf('bkg').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit'))
				nDOF = np.append( nDOF , testBkgWS.pdf('bkg').getVariables().getSize() )
				allChiSqrd = np.append( allChiSqrd , testFrame.chiSquare( int(nDOF[i]) ) )


			bestFunctionNum = np.where( np.abs(1.0 - allChiSqrd ) == np.abs(1.0 - allChiSqrd ).min() )[0][0]
			whichFunction = myBkgFunctions[bestFunctionNum][0]

			print("Found best functional form: ",whichFunction)

			### Create workspaces containing the specified signal and background shapes

			# Make a workspace to hold the smooth, analytic approximation of the bkg shape

			basisBkgWS = generateBackgroundWorkspace( "backgroundWS" , whichFunction , [ xMin , xMax ] )
			inRooHisto = ROOT.RooDataHist("inRooHisto", "inRooHisto", ROOT.RooArgList(basisBkgWS.obj("rooMyy")), inHisto)
			basisBkgWS.obj("bkg").fitTo( inRooHisto , ROOT.RooFit.PrintLevel(0) , ROOT.RooFit.PrintEvalErrors(2) , ROOT.RooFit.SumW2Error(ROOT.kTRUE) )


		### We're finally about ready to run some toys with the shapes we've found/generated

		# Get some statistics information - will try to use an "average" weight to make sure toy weighting matches input histo weighting

		ratio_stat_err = []
		for ibin in range(nBins):
			if( inHisto.GetBinError(ibin+1) == 0 ): continue
			if( inHisto.GetBinContent(ibin+1) == 0 ): continue
			ratio_stat_err.append( ( inHisto.GetBinError(ibin+1) / np.sqrt( inHisto.GetBinContent(ibin+1) ) )**2 )
		avg_wgt = np.mean( ratio_stat_err )

		n_sig_unwgt = int( np.round( sigYield / avg_wgt ) )
		n_bkg_unwgt = int( np.round( bkgYield / avg_wgt ) )

		print("Info: Found average weight = ",avg_wgt," in input template.")

		if( useToyYieldOverride ):
			n_bkg_unwgt = toyYieldOverrides[iCat]
			print("INFO: User has opted to override the raw number of Background events in each toy.")
			print("      N_bkg = ",n_bkg_unwgt)
			bkgYield = n_bkg_unwgt * avg_wgt
			if(useSignalFracs):
				n_sig_unwgt = max( np.round( n_bkg_unwgt * signalInjectFractions[iCat] ) , 1.0)
				sigYield = n_sig_unwgt * avg_wgt

		### Perform the toy tests
		if( not isPrepMode ):

			nToys = userin.nSignalInjectToys

			### Make holders for the toy test outputs (will need one per tested bkg function)

			myBkgFunctions = define_background_functions()

			outputTrees = []

			b_nBkgEvts = []
			b_nSigEvts = []

			b_sigMass = []
			b_sigPeak = []
			b_sigWidth = []

			b_spurSigGPRValue = []
			b_spurSigToyValue = []
			b_spurSigGPRValue_abs = []
			b_spurSigToyValue_abs = []

			b_GPR_fitStatus = []
			b_Toy_fitStatus = []

			b_GPR_bkgFitPars = []
			b_Toy_bkgFitPars = []

			b_GPR_sbFitVals = []
			b_Toy_sbFitVals = []

			b_GPR_deltaS = []
			b_GPR_deltaB = []

			b_Toy_deltaS = []
			b_Toy_deltaB = []

			for ifunc,funcDef in enumerate(myBkgFunctions):

				testFunction = funcDef[0]

				### Define the tree and branches needed for the outputs

				treeName = inHistName+"_ToyTests_func_"+testFunction
				outputTrees.append( ROOT.TTree(treeName,treeName) )
				outputTrees[-1].SetDirectory( outToyTestFile )

				b_spurSigGPRValue.append( np.zeros(1, dtype=float) )
				b_spurSigToyValue.append( np.zeros(1, dtype=float) )
				b_spurSigGPRValue_abs.append( np.zeros(1, dtype=float) )
				b_spurSigToyValue_abs.append( np.zeros(1, dtype=float) )
				b_GPR_fitStatus.append( np.zeros(1, dtype=float) )
				b_Toy_fitStatus.append( np.zeros(1, dtype=float) )
				b_GPR_bkgFitPars.append( np.zeros(5, dtype='d') )
				b_Toy_bkgFitPars.append( np.zeros(5, dtype='d') )
				b_GPR_sbFitVals.append( np.zeros(nBins, dtype='d') )
				b_Toy_sbFitVals.append( np.zeros(nBins, dtype='d') )
				b_GPR_deltaS.append( np.zeros(1, dtype=float) )
				b_GPR_deltaB.append( np.zeros(1, dtype=float) )
				b_Toy_deltaS.append( np.zeros(1, dtype=float) )
				b_Toy_deltaB.append( np.zeros(1, dtype=float) )
				b_sigMass.append( np.zeros(1, dtype=float) )
				b_sigPeak.append( np.zeros(1, dtype=float) )
				b_sigWidth.append( np.zeros(1, dtype=float) )


				outputTrees[-1].Branch("nSpurSigValue_GPR",b_spurSigGPRValue[-1],"nSpurSigValue_GPR/D")
				outputTrees[-1].Branch("nSpurSigValue_Toy",b_spurSigToyValue[-1],"nSpurSigValue_Toy/D")
				outputTrees[-1].Branch("nSpurSigAbsValue_GPR",b_spurSigGPRValue_abs[-1],"nSpurSigAbsValue_GPR/D")
				outputTrees[-1].Branch("nSpurSigAbsValue_Toy",b_spurSigToyValue_abs[-1],"nSpurSigAbsValue_Toy/D")
				outputTrees[-1].Branch("GPR_fitStatus",b_GPR_fitStatus[-1],"GPR_fitStatus/D")
				outputTrees[-1].Branch("Toy_fitStatus",b_Toy_fitStatus[-1],"Toy_fitStatus/D")
				outputTrees[-1].Branch("GPR_bkgFitPars",b_GPR_bkgFitPars[-1],"GPR_bkgFitPars[5]/D")
				outputTrees[-1].Branch("Toy_bkgFitPars",b_Toy_bkgFitPars[-1],"Toy_bkgFitPars[5]/D")
				outputTrees[-1].Branch("GPR_sbFitVals",b_GPR_sbFitVals[-1],"GPR_sbFitVals["+str(nBins)+"]/D")
				outputTrees[-1].Branch("Toy_sbFitVals",b_Toy_sbFitVals[-1],"Toy_sbFitVals["+str(nBins)+"]/D")
				outputTrees[-1].Branch("GPR_deltaS",b_GPR_deltaS[-1],"GPR_deltaS/D")
				outputTrees[-1].Branch("GPR_deltaB",b_GPR_deltaB[-1],"GPR_deltaB/D")
				outputTrees[-1].Branch("Toy_deltaS",b_Toy_deltaS[-1],"Toy_deltaS/D")
				outputTrees[-1].Branch("Toy_deltaB",b_Toy_deltaB[-1],"Toy_deltaB/D")
				outputTrees[-1].Branch("sigMass",b_sigMass[-1],"sigMass/D")
				outputTrees[-1].Branch("sigPeak",b_sigPeak[-1],"sigPeak/D")
				outputTrees[-1].Branch("sigWidth",b_sigWidth[-1],"sigWidth/D")


			# Also make a tree for "generic" info (which doesn't depend on the spurious signal fit)

			treeName = inHistName+"_ToyTests_gen"
			genericOutTree = ROOT.TTree(treeName,treeName)
			genericOutTree.SetDirectory( outToyTestFile )

			b_nBkgEvts = np.zeros(1, dtype=float)
			b_nUnweightedBkgEvts = np.zeros(1, dtype=float)
			b_nSigEvts = np.zeros(1, dtype=float)
			b_GPR_histVals = np.zeros(nBins, dtype=float)
			b_Toy_histVals = np.zeros(nBins, dtype=float)
			b_binning = np.zeros(nBins+1, dtype=float)
			b_GP_length_scale = np.zeros(1, dtype=float)

			genericOutTree.Branch("nBkgEvts",b_nBkgEvts,"nBkgEvts/D")
			genericOutTree.Branch("nUnweightedBkgEvts",b_nUnweightedBkgEvts,"nUnweightedBkgEvts/D")
			genericOutTree.Branch("nSigEvts",b_nSigEvts,"nSigEvts/D")
			genericOutTree.Branch("GPR_histVals",b_GPR_histVals,"GPR_histVals["+str(nBins)+"]/D")
			genericOutTree.Branch("Toy_histVals",b_Toy_histVals,"Toy_histVals["+str(nBins)+"]/D")
			genericOutTree.Branch("binning",b_binning,"binning["+str(nBins+1)+"]/D")
			genericOutTree.Branch("GP_length_scale",b_GP_length_scale,"GP_length_scale/D")

			# Ensure the random seed is not the same between different batch jobs (ROOT always uses the same default seed on starting)
			seednum = rand.randint(0,99999)
			ROOT.RooRandom.randomGenerator().SetSeed(seednum)

			print("About to run toys...")

			for itoy in range( nToys ):

				myToy = ROOT.TH1D("myToy","myToy",nBins,myBins.array())

				if( not userin.useSmoothToys ):
					myToy = fluctuateTemplate( inHisto )
				else:
					mybkg = basisBkgWS.pdf("bkg").generate( ROOT.RooArgSet( basisBkgWS.var('rooMyy') ) , n_bkg_unwgt )
					myBkgHisto = ROOT.RooAbsData.createHistogram( mybkg, 'myBkgHisto' , mybkg.get().find('rooMyy') , ROOT.RooFit.Binning(myBins) ) #ROOT.RooFit.SumW2Error(ROOT.kTRUE)
					myBkgHisto.Scale( bkgYield / myBkgHisto.Integral() )
					myToy.Add( myBkgHisto )

				if( sigYield > 0 ):
					basisSigWS.var(sigPDF_posName).setVal( signalPos )
					if( useToySigPosOverride ): 
						basisSigWS.var(sigPDF_posName).setVal( toyInjSigPosOverrides[iCat] )
					if( not isMassDepWidth ):
						basisSigWS.var(sigPDF_widthName).setVal( signalWidth )
					mysig = basisSigWS.pdf(sigPDF_name).generate( ROOT.RooArgSet( basisSigWS.var('rooMyy') ) , n_sig_unwgt )
					mySigHisto = ROOT.RooAbsData.createHistogram( mysig, 'mySigHisto' , mysig.get().find('rooMyy') , ROOT.RooFit.Binning(myBins) ) #ROOT.RooFit.SumW2Error(ROOT.kTRUE)
					mySigHisto.Scale( sigYield / mySigHisto.GetSumOfWeights() )
					myToy.Add( mySigHisto )

				#for ibin in range(nBins): myToy.SetBinError( ibin+1 , inHisto.GetBinError(ibin+1) )

				# For debugging
# 				c = ROOT.TCanvas('c','c',600,600)
# 				scaledInHisto = inHisto.Clone("scaledInHisto")
# 				scaledInHisto.Scale( bkgYield / scaledInHisto.Integral() )
# 				scaledInHisto.SetLineColor(1)
# 				scaledInHisto.SetMarkerColor(1)
# 				scaledInHisto.Draw('ehist')
# 				myToy.SetLineColor(4)
# 				myToy.SetMarkerColor(4)
# 				myToy.SetMarkerStyle(4)
# 				myToy.Draw('ephist same')
# 				basisBkgHisto = basisBkgWS.obj('bkg').createHistogram("basisBkgHisto",basisBkgWS.obj("rooMyy"),ROOT.RooFit.Binning(myBins) , ROOT.RooFit.Normalization(bkgYield, ROOT.RooAbsReal.NumEvent) )
# 				basisBkgHisto.Scale( bkgYield / basisBkgHisto.GetSumOfWeights() )
# 				basisBkgHisto.SetLineColor(2)
# 				basisBkgHisto.SetMarkerColor(2)
# 				basisBkgHisto.Draw('hist same')
# 				c.Update()
# 				import pdb; pdb.set_trace()


				# Perform the GP fit to obtain the smoothed toy template

				print("About to perform GP Fit to Toy #",itoy)

				smoothedTemplate , gp_fit = makeGPtemplate( myToy ,
												userin.whichKernel ,
												length_scale ,
												length_scale_bounds ,
												length_scale_slope ,
												length_scale_slope_bounds ,
												whichGPMean ,
												nBinsToPad ,
												whichErrorTreatment ,
												whichErrorOutput ,
												calculateSystematics=False ,
												systVarFrac=0.0 )

				print("Done with GP Fit to Toy #",itoy," - will now calculate spurious signal.")


				### Perform the spurious signal test for the raw and GP templates

				if( not toyMassScanPoints ): toyMassScanPoints = [ signalPos ]

				for ifunc,funcDef in enumerate(myBkgFunctions):

					testFunction = funcDef[0]
					print("   Performing Spurious Signal Test with ",testFunction," Function.")
					testBkgWS = generateBackgroundWorkspace( "test"+testFunction+"WS" , testFunction , [ xMin , xMax ] )

					for imass,mass in enumerate(toyMassScanPoints):

						basisSigWS.var(sigPDF_posName).setVal( mass )
						if( not isMassDepWidth ):
							basisSigWS.var(sigPDF_widthName).setVal( signalWidth )

						b_sigMass[ifunc][0] = mass
						b_sigPeak[ifunc][0] = basisSigWS.obj(sigPDF_posName).getValV()
						b_sigWidth[ifunc][0] = basisSigWS.obj(sigPDF_widthName).getValV()

						fittedSignal_Raw , fitStatus_Raw , bkgFitParams_Raw , sbFitVals_Raw , fitSerr_Raw , fitBerr_Raw = performSBFit( myToy , basisSigWS , sigYield , testBkgWS , bkgYield , sbFitMassRange )
						fittedSignal_GP , fitStatus_GP , bkgFitParams_GP , sbFitVals_GP , fitSerr_GP , fitBerr_GP = performSBFit( smoothedTemplate , basisSigWS , sigYield , testBkgWS , bkgYield , sbFitMassRange )

						b_spurSigGPRValue[ifunc][0] = fittedSignal_GP
						b_spurSigGPRValue_abs[ifunc][0] = np.abs( fittedSignal_GP )
						b_spurSigToyValue[ifunc][0] = fittedSignal_Raw
						b_spurSigToyValue_abs[ifunc][0] = np.abs( fittedSignal_Raw )

						b_GPR_deltaS[ifunc][0] = fitSerr_GP
						b_GPR_deltaB[ifunc][0] = fitBerr_GP

						b_Toy_deltaS[ifunc][0] = fitSerr_Raw
						b_Toy_deltaB[ifunc][0] = fitBerr_Raw

						b_GPR_fitStatus[ifunc][0] = fitStatus_GP
						b_Toy_fitStatus[ifunc][0] = fitStatus_Raw

						# fill the fit parameters from the bkg fit to the toys
						for ipar in range(len(bkgFitParams_GP)):
							b_GPR_bkgFitPars[ifunc][ipar] = bkgFitParams_GP[ipar]
							b_Toy_bkgFitPars[ifunc][ipar] = bkgFitParams_Raw[ipar]

						# fill the S+B fit contents (bin by bin)
						for ibin in range(len(sbFitVals_GP)):
							b_GPR_sbFitVals[ifunc][ibin] = sbFitVals_GP[ibin]
							b_Toy_sbFitVals[ifunc][ibin] = sbFitVals_Raw[ibin]


						outputTrees[ifunc].Fill()


				### Save both the toy and GP-smoothed templates to the output file

				b_nBkgEvts[0] = bkgYield   #inHisto.Integral()
				b_nUnweightedBkgEvts[0] = n_bkg_unwgt
				b_nSigEvts[0] = sigYield

				for ibin in range(nBins):
					b_GPR_histVals[ibin] = smoothedTemplate.GetBinContent( ibin+1 )
					b_Toy_histVals[ibin] = myToy.GetBinContent( ibin+1 )
					b_binning[ibin] = myToy.GetBinLowEdge( ibin+1 )

				b_binning[-1] = myToy.GetBinLowEdge( nBins+1 )

				params = gp_fit.kernel_.get_params()
				b_GP_length_scale[0] = -1.0
				for par in params:
					if( ( "l0" in par ) or ( "length_scale" in par ) ):
						if( not "bounds" in par ):
							b_GP_length_scale[0] = params[par]

				genericOutTree.Fill()

			### Save the smoothed shape used to generate the toys

			outToyTestFile.cd()
			if( not userin.useSmoothToys ):
				truthHistName = inHistName+"_ToyBasis"
				basisBkgHisto = inHisto.Clone(truthHistName)
				basisBkgHisto.SetDirectory(outToyTestFile)
				basisBkgHisto.Write()

			else:
				basisBkgHisto = basisBkgWS.obj('bkg').createHistogram("basisBkgHisto",basisBkgWS.obj("rooMyy"),ROOT.RooFit.Binning(myBins) ) #,ROOT.RooFit.Normalization(bkgYield, ROOT.RooAbsReal.NumEvent) )
				basisBkgHisto.Scale( bkgYield / basisBkgHisto.GetSumOfWeights() )
				truthHistName = inHistName+"_ToyBasis"
				basisBkgHisto.SetName( truthHistName )

				if( sigYield > 0 ):
					basisSigHisto = basisSigWS.obj(sigPDF_name).createHistogram("basisSigHisto",basisSigWS.obj("rooMyy"),ROOT.RooFit.Binning(myBins) ) #,ROOT.RooFit.Normalization(bkgYield, ROOT.RooAbsReal.NumEvent) )
					basisSigHisto.Scale( sigYield / basisSigHisto.GetSumOfWeights() )
					basisBkgHisto.Add(basisSigHisto)

				for ibin in range(nBins): basisBkgHisto.SetBinError( ibin+1 , inHisto.GetBinError(ibin+1) )

				basisBkgHisto.SetDirectory(outToyTestFile)
				basisBkgHisto.Write()

# 			can = ROOT.TCanvas("can","can",600,600)
# 			basisBkgHisto.Draw()
# 			basisSigHisto.Draw("same")
# 			import pdb; pdb.set_trace()

			### Save the Trees to a ROOT file

			outToyTestFile.cd()
			for tree in outputTrees:
				tree.Write()
			genericOutTree.Write()

			outToyTestFile.Write()


		else:

			# Save the "basis" functional form to the outToyBasisFile

			basisBkgWS.SetName( "basisBkgWS_"+str(inHistName) )
			outToyBasisFile.cd()
			basisBkgWS.Write()

			# More Debugging Plots
# 			can = ROOT.TCanvas("can","can",600,600)
# 			inHisto.SetMarkerColor(1)
# 			inHisto.SetMarkerStyle(8)
# 			inHisto.Draw("ep")
# 			basisBkgHisto = basisBkgWS.obj('bkg').createHistogram("basisBkgHisto",basisBkgWS.obj("rooMyy"),ROOT.RooFit.Binning(myBins) , ROOT.RooFit.Normalization(bkgYield, ROOT.RooAbsReal.NumEvent) )
# 			basisBkgHisto.Scale( bkgYield / basisBkgHisto.GetSumOfWeights() )
# 			basisBkgHisto.SetLineColor(2)
# 			basisBkgHisto.Draw("hist same")
# 			can.Update()
# 			import pdb; pdb.set_trace()





if __name__== "__main__":

	parser = argparse.ArgumentParser()
	#parser.add_argument("-c","--config", help="Which input configuration file to use, along with the path to said file. The default is inc/inputs.yaml.",default='inc/inputs.yaml')
	parser.add_argument("--generateToyBasisFile", help="Create a file containing the analytic function fits to the input templates. No toy tests will be run.",action="store_true")
	args = parser.parse_args()
	isPrepMode = args.generateToyBasisFile

	#configfilename = args.config
	#userin = yaml.safe_load(open(configfilename))

	main( isPrepMode )
