#!/bin/sh
date
echo "Running on the host: `/bin/hostname`"

#################### ANALYSIS SHELL SCRIPT ########################

echo "Current directory: `pwd`"

echo 'Starting...'

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt Python"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt ROOT"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt numpy"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt uproot"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scikitlearn"
lsetup "lcgenv -p LCG_97python3 x86_64-centos7-gcc8-opt scipy"

cp -r /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/inc .
cp /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/example_template.root .
cp /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/myExample_ToyBasis.root .
cp /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/myExampleHyperParOptimization.root .
cp /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/myExampleSignalShapeWS.root .
cp /afs/cern.ch/work/r/rahynema/public/test_gpr/gasbag/toyTest.py .

python3 -b toyTest.py

echo 'Ending...'

rm -rf ./inc 
rm example_template.root
rm myExample_ToyBasis.root 
rm myExampleHyperParOptimization.root
rm myExampleSignalShapeWS.root
rm toyTest.py

###############################################################

echo "Done."
